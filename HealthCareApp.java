package com.vaibhav.healthcare;

import com.vaibhav.healthcare.customerdetails.CustomerCurrentHealth;
import com.vaibhav.healthcare.customerdetails.CustomerDetails;
import com.vaibhav.healthcare.customerdetails.CustomerHabits;

public class HealthCareApp {

	public static void main(String[] args) {
		HealthCareApp healthCareApp = new HealthCareApp();
		CustomerDetails customerDetails = healthCareApp.getCustomerDetails();
		double insurancePremium = healthCareApp.calculateInsurancePremium(customerDetails);
		System.out.println("13 : " + insurancePremium);

	}

	public double calculateInsurancePremium(CustomerDetails customerDetails) {

		double basePremium = 5000;
		double finalPremium;

		System.out.println("22 : " + basePremium);
		System.out.println();

		/*
		 * if (customerDetails.getAge() < 18) { finalPremium = basePremium; }
		 */
		System.out.println("25 : " + customerDetails.getAge());

		if (customerDetails.getAge() >= 18 && customerDetails.getAge() < 25) {
			basePremium = basePremium + (basePremium * 10.00 / 100);
		}
		if (customerDetails.getAge() >= 25 && customerDetails.getAge() < 30) {
			basePremium = basePremium + (basePremium * 20 / 100);
		}
		if (customerDetails.getAge() >= 30 && customerDetails.getAge() < 35) {
			System.out.println("37 : " + basePremium);
			basePremium = basePremium + (basePremium * 30 / 100);
			System.out.println("39 : " + basePremium);
		}
		if (customerDetails.getAge() >= 35 && customerDetails.getAge() < 40) {
			basePremium = basePremium + (basePremium * 40 / 100);
		}
		if (customerDetails.getAge() >= 40) {
			basePremium = basePremium + (basePremium * 60 / 100);
		}
		System.out.println("42 : " + basePremium);

		//finalPremium = basePremium;

		switch (customerDetails.getGender()) {
		
		case "Male":
			basePremium = basePremium + (basePremium * 2 / 100);
			break;
		
		case "Female":
			basePremium = basePremium + (basePremium * 4 / 100);
			break;

		case "Other":
			basePremium = basePremium + (basePremium * 4 / 100);
			break;

		default:
			break;
		}
		System.out.println("63 : " + basePremium);
		//finalPremium = basePremium;

		if (customerDetails.getCustCurrentHealth().isHypertension()) {
			basePremium = basePremium + (basePremium * 1 / 100);
		}
		if (customerDetails.getCustCurrentHealth().isBloodPressure()) {
			basePremium = basePremium + (basePremium * 1 / 100);
		}
		if (customerDetails.getCustCurrentHealth().isBloodSugar()) {
			basePremium = basePremium + (basePremium * 1 / 100);
		}
		if (customerDetails.getCustCurrentHealth().isOverWeight()) {
			basePremium = basePremium + (basePremium * 1 / 100);
		}
		System.out.println("83 : " + basePremium);

		if (customerDetails.getCustHabits().isSmoking()) {
			basePremium = basePremium + (basePremium * 3 / 100);
		}
		if (customerDetails.getCustHabits().isAlcohol()) {
			basePremium = basePremium + (basePremium * 3 / 100);
		}
		if (customerDetails.getCustHabits().isDrugs()) {
			basePremium = basePremium + (basePremium * 3 / 100);
		}
		if (customerDetails.getCustHabits().isDailyExercise()) {
			basePremium = basePremium - (basePremium * 3 / 100);
		}
		
		System.out.println("98 : " + basePremium);

		finalPremium = basePremium;
		return finalPremium;

	}

	private CustomerDetails getCustomerDetails() {
		CustomerDetails customerDetails = new CustomerDetails();

		customerDetails.setName("Norman Gomes");
		customerDetails.setAge(34);
		customerDetails.setGender("Male");

		CustomerCurrentHealth customerCurrentHealth = new CustomerCurrentHealth();
		customerCurrentHealth.setHypertension(false);
		customerCurrentHealth.setBloodPressure(false);
		customerCurrentHealth.setBloodSugar(false);
		customerCurrentHealth.setOverWeight(true);

		CustomerHabits customerHabits = new CustomerHabits();
		customerHabits.setSmoking(false);
		customerHabits.setAlcohol(true);
		customerHabits.setDailyExercise(true);
		customerHabits.setDrugs(false);

		customerDetails.setCustCurrentHealth(customerCurrentHealth);
		customerDetails.setCustHabits(customerHabits);

		return customerDetails;
	}

}
