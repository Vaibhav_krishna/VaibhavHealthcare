package com.vaibhav.healthcare.customerdetails;

public class CustomerCurrentHealth {

	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overWeight;

	/**
	 * @return the hypertension
	 */
	public boolean isHypertension() {
		return hypertension;
	}

	/**
	 * @param hypertension
	 *            the hypertension to set
	 */
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}

	/**
	 * @return the bloodPressure
	 */
	public boolean isBloodPressure() {
		return bloodPressure;
	}

	/**
	 * @param bloodPressure
	 *            the bloodPressure to set
	 */
	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	/**
	 * @return the bloodSugar
	 */
	public boolean isBloodSugar() {
		return bloodSugar;
	}

	/**
	 * @param bloodSugar
	 *            the bloodSugar to set
	 */
	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	/**
	 * @return the overWeight
	 */
	public boolean isOverWeight() {
		return overWeight;
	}

	/**
	 * @param overWeight
	 *            the overWeight to set
	 */
	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}

}
