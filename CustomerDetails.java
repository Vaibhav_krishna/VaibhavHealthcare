package com.vaibhav.healthcare.customerdetails;

public class CustomerDetails {

	public CustomerDetails() {
	}

	public CustomerDetails(String name, int age, String gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	private String name;
	private int age;
	private String gender;

	private CustomerCurrentHealth custCurrentHealth;
	private CustomerHabits custHabits;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the custCurrentHealth
	 */
	public CustomerCurrentHealth getCustCurrentHealth() {
		return custCurrentHealth;
	}

	/**
	 * @param custCurrentHealth
	 *            the custCurrentHealth to set
	 */
	public void setCustCurrentHealth(CustomerCurrentHealth custCurrentHealth) {
		this.custCurrentHealth = custCurrentHealth;
	}

	/**
	 * @return the custHabits
	 */
	public CustomerHabits getCustHabits() {
		return custHabits;
	}

	/**
	 * @param custHabits
	 *            the custHabits to set
	 */
	public void setCustHabits(CustomerHabits custHabits) {
		this.custHabits = custHabits;
	}
}
